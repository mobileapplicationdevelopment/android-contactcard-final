package com.luukros.contactcard;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
        RandomUserAsyncTask.onRandomUserAvailable,
        View.OnClickListener,
        AdapterView.OnItemClickListener {

    private FloatingActionButton mAddRandomUserBtn;
    private RandomUserAdapter mRandomUserAdapter = null;

    RandomUserDB mRandomUserDB;
    ArrayList<RandomUser> mRandomUserArrayList = new ArrayList<RandomUser>();

    private ListView mRandomUsersListView;
    private EditText mSearchRandomUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRandomUsersListView = (ListView) findViewById(R.id.randomUsersListView);
        mRandomUsersListView.setOnItemClickListener(this);

        mRandomUserDB = new RandomUserDB(this);
        mRandomUserArrayList = mRandomUserDB.getRandomUsers();

        mSearchRandomUsers = (EditText) findViewById(R.id.randomUsersEditText);
        mSearchRandomUsers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Auto-generated
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

                int textLength = charSequence.length();

                ArrayList<RandomUser> tempArrayList = new ArrayList<RandomUser>();

                for (RandomUser randomUser : mRandomUserArrayList) {

                    if (textLength <= randomUser.getRandomUser().length()) {

                        if (randomUser.getRandomUser().contains(charSequence.toString().toLowerCase())) {
                            tempArrayList.add(randomUser);
                        }

                    }

                }

                mRandomUserAdapter = new RandomUserAdapter(MainActivity.this, getLayoutInflater(), tempArrayList);
                mRandomUsersListView.setAdapter(mRandomUserAdapter);

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Auto-generated
            }
        });

        mRandomUserAdapter = new RandomUserAdapter(getApplicationContext(),
                getLayoutInflater(),
                mRandomUserArrayList);
        mRandomUsersListView.setAdapter(mRandomUserAdapter);

        mAddRandomUserBtn = (FloatingActionButton) findViewById(R.id.addPersonButton);
        mAddRandomUserBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        RandomUserAsyncTask getRandomUser = new RandomUserAsyncTask(this, getApplicationContext());
        String [] url = new String[] { "https://randomuser.me/api/" };
        getRandomUser.execute(url);
    }

    @Override
    public void onRandomUserAvailable(RandomUser randomUser) {

        mRandomUserArrayList.add(randomUser);
        mRandomUserAdapter.notifyDataSetChanged();

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);

        RandomUser randomUser = (RandomUser) this.mRandomUserArrayList.get(position);

        intent.putExtra("RandomUser", randomUser);

        startActivity(intent);

    }

}
