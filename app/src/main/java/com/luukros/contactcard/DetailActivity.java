package com.luukros.contactcard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ImageView userThumbnail = (ImageView) findViewById(R.id.userThumbnail);
        TextView userFullName   = (TextView) findViewById(R.id.userFullName);
        TextView phoneNumber    = (TextView) findViewById(R.id.phoneInput);
        TextView cellNumber     = (TextView) findViewById(R.id.cellInput);
        TextView email          = (TextView) findViewById(R.id.emailInput);
        TextView street         = (TextView) findViewById(R.id.streetInput);
        TextView city           = (TextView) findViewById(R.id.cityInput);
        TextView state          = (TextView) findViewById(R.id.stateInput);
        TextView postcode       = (TextView) findViewById(R.id.postcodeInput);

        Intent i = getIntent();
        RandomUser randomUser = (RandomUser) i.getSerializableExtra("RandomUser");

        if(randomUser.mUserThumbnail != "") {
            Picasso.with(getApplicationContext()).load(randomUser.mUserThumbnail).into(userThumbnail);
        }

        userFullName.setText(randomUser.mTitle + " " + randomUser.mFirstName + " " + randomUser.mLastName);
        phoneNumber.setText(randomUser.mPhoneNumber);
        cellNumber.setText(randomUser.mCellNumber);
        email.setText(randomUser.mEmail);
        street.setText(randomUser.mStreet);
        city.setText(randomUser.mCity);
        state.setText(randomUser.mState);
        postcode.setText(String.valueOf(randomUser.mPostcode));

    }

}
