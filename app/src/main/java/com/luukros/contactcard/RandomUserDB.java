package com.luukros.contactcard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Luuk on 6-10-2016.
 */

public class RandomUserDB extends SQLiteOpenHelper {

    // Defining DB_VERSION and DB_NAME
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "random_users";

    // Defining TABLE_NAME
    private static final String TABLE_NAME = "random_users";

    // Defining all columns in TABLE
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_THUMBNAIL = "thumbnail";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_FIRSTNAME = "firstName";
    private static final String COLUMN_LASTNAME = "lastName";
    private static final String COLUMN_PHONE = "phoneNumber";
    private static final String COLUMN_CELL = "cellNumber";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_STREET = "street";
    private static final String COLUMN_CITY= "city";
    private static final String COLUMN_STATE = "state";
    private static final String COLUMN_POSTCODE = "postcode";

    // Constructor for DB-class
    RandomUserDB(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_RANDOMUSER_TABLE = "CREATE TABLE " + TABLE_NAME +
                "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    COLUMN_THUMBNAIL + " TEXT, " +
                    COLUMN_TITLE + " TEXT, " +
                    COLUMN_FIRSTNAME + " TEXT, " +
                    COLUMN_LASTNAME + " TEXT, " +
                    COLUMN_PHONE + " TEXT, " +
                    COLUMN_CELL + " TEXT, " +
                    COLUMN_EMAIL + " TEXT, " +
                    COLUMN_STREET + " TEXT, " +
                    COLUMN_CITY + " TEXT, " +
                    COLUMN_STATE + " TEXT, " +
                    COLUMN_POSTCODE + " INTEGER " +
                ");";

        db.execSQL(CREATE_RANDOMUSER_TABLE);
        Log.i("DATABASE CREATED", "YAY");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addRandomUser(RandomUser randomUser) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_THUMBNAIL, randomUser.getUserThumbnail());
        contentValues.put(COLUMN_TITLE, randomUser.getTitle());
        contentValues.put(COLUMN_FIRSTNAME, randomUser.getFirstName());
        contentValues.put(COLUMN_LASTNAME, randomUser.getLastName());
        contentValues.put(COLUMN_PHONE, randomUser.getPhoneNumber());
        contentValues.put(COLUMN_CELL, randomUser.getCellNumber());
        contentValues.put(COLUMN_EMAIL, randomUser.getEmail());
        contentValues.put(COLUMN_STREET, randomUser.getStreet());
        contentValues.put(COLUMN_CITY, randomUser.getCity());
        contentValues.put(COLUMN_STATE, randomUser.getState());
        contentValues.put(COLUMN_POSTCODE, randomUser.getPostcode());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_NAME, null, contentValues);
        db.close();

    }

    public ArrayList<RandomUser> getRandomUsers() {

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<RandomUser> randomUsers = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        cursor.moveToFirst();
        db.close();

        if (cursor.moveToFirst()) {

            while (!cursor.isAfterLast()) {

                RandomUser randomUser = new RandomUser();
                randomUser.mUserThumbnail = cursor.getString(cursor.getColumnIndex(COLUMN_THUMBNAIL));
                randomUser.mTitle = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE));
                randomUser.mFirstName = cursor.getString(cursor.getColumnIndex(COLUMN_FIRSTNAME));
                randomUser.mLastName = cursor.getString(cursor.getColumnIndex(COLUMN_LASTNAME));
                randomUser.mPhoneNumber = cursor.getString(cursor.getColumnIndex(COLUMN_PHONE));
                randomUser.mCellNumber = cursor.getString(cursor.getColumnIndex(COLUMN_CELL));
                randomUser.mEmail = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
                randomUser.mStreet = cursor.getString(cursor.getColumnIndex(COLUMN_STREET));
                randomUser.mCity = cursor.getString(cursor.getColumnIndex(COLUMN_CITY));
                randomUser.mState = cursor.getString(cursor.getColumnIndex(COLUMN_STATE));
                randomUser.mPostcode = cursor.getInt(cursor.getColumnIndex(COLUMN_POSTCODE));

                randomUsers.add(randomUser);
                cursor.moveToNext();

            }

        }

        return randomUsers;

    }

}
