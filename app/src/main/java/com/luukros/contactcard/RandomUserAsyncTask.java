package com.luukros.contactcard;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Luuk on 3-10-2016.
 */

public class RandomUserAsyncTask extends AsyncTask<String, Void, String> {

    private onRandomUserAvailable listener = null;

    private static final String TAG = "RandomUserAsyncTask";
    private static final String requestUrl = "https://randomuser.me/api/";

    Context mContext;
    RandomUserDB mRandomUserDB;

    public RandomUserAsyncTask(onRandomUserAvailable listener, Context c) {
        this.listener = listener;
        mContext = c;
    }

    @Override
    protected String doInBackground(String... params) {

        InputStream inputStream = null;

        int responseCode = -1;
        String response = "";

        for (String url : params) {
            Log.i(TAG, url);
        }

        try {

            URL url = new URL(requestUrl);
            URLConnection urlConnection = url.openConnection();

            if (!(urlConnection instanceof HttpURLConnection)) {
                return null;
            }

            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            httpURLConnection.setAllowUserInteraction(false);
            httpURLConnection.setInstanceFollowRedirects(true);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            responseCode = httpURLConnection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {

                inputStream = httpURLConnection.getInputStream();
                response = getStringFromInputStream(inputStream);

            }

        } catch (MalformedURLException e) {

            Log.e("TAG", e.getLocalizedMessage());
            e.printStackTrace();
            return null;

        } catch (IOException e) {

            Log.e("TAG", e.getLocalizedMessage());
            e.printStackTrace();
            return null;

        }

        return response;

    }

    protected void onPostExecute(String response) {

        JSONObject jsonObject;

        try {

            // This represents the top-level JSON-object
            jsonObject = new JSONObject(response);

            mRandomUserDB = new RandomUserDB(mContext);

            // This represents the actual results-array
            JSONArray results = jsonObject.getJSONArray("results");

            for (int i = 0; i < results.length(); i++) {

                // This represents a individual RandomUser inside the results-array
                JSONObject randomUsers = results.getJSONObject(i);

                // This represents the name-object in a RandomUser
                JSONObject nameObject = randomUsers.getJSONObject("name");
                // The following lines get Strings from the name-object
                String title        = nameObject.getString("title");
                String firstName    = nameObject.getString("first");
                String lastName     = nameObject.getString("last");
                Log.i(TAG, title + " " + firstName + " " + lastName);

                // The following lines get Strings from multiple contact-related objects
                String email        = randomUsers.getString("email");
                String phoneNumber  = randomUsers.getString("phone");
                String cellNumber   = randomUsers.getString("cell");
                Log.i(TAG, email + "\n" + phoneNumber + "\n" + cellNumber);

                // This represents the location-object in a Random User
                JSONObject locationObject = randomUsers.getJSONObject("location");
                // The following lines get Strings from multiple location-related objects
                String street       = locationObject.getString("street");
                String city         = locationObject.getString("city");
                String state        = locationObject.getString("state");
//                int postcode        = locationObject.getInt("postcode");
                Log.i(TAG, street + "\n" + city + "\n" + state + "\n");

                // This represents the picture-object in a RandomUser
                JSONObject imageObject = randomUsers.getJSONObject("picture");
                // The following line gets a String (the url) from the picture-object
                String thumbnail = imageObject.getString("large");
                Log.i(TAG, thumbnail);

                // Creating a new RandomUser-object
                RandomUser randomUser       = new RandomUser();
                randomUser.mTitle           = title;
                randomUser.mFirstName       = firstName;
                randomUser.mLastName        = lastName;
                randomUser.mEmail           = email;
                randomUser.mPhoneNumber     = phoneNumber;
                randomUser.mCellNumber      = cellNumber;
                randomUser.mStreet          = street;
                randomUser.mCity            = city;
                randomUser.mState           = state;
//                randomUser.mPostcode        = postcode;
                randomUser.mUserThumbnail   = thumbnail;

                listener.onRandomUserAvailable(randomUser);

                mRandomUserDB.addRandomUser(randomUser);
                Log.i(TAG, "added random user");

            }

        } catch (JSONException e) {
            Log.e("TAG", e.getLocalizedMessage());
            e.printStackTrace();
        }

    }

    private static String getStringFromInputStream(InputStream inputStream) {

        BufferedReader bufferedReader = null;
        StringBuilder stringBuilder = new StringBuilder();

        String line;

        try {

            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

        } catch (IOException e) {

            Log.e("TAG", e.getLocalizedMessage());
            e.printStackTrace();

        } finally {

            if (bufferedReader != null) {

                try {

                    bufferedReader.close();

                } catch (IOException e) {

                    Log.e("TAG", e.getLocalizedMessage());
                    e.printStackTrace();

                }

            }

        }

        return stringBuilder.toString();

    }

    public interface onRandomUserAvailable {
        void onRandomUserAvailable(RandomUser randomUser);
    }







































}
