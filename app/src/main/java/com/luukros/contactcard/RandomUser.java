package com.luukros.contactcard;

import java.io.Serializable;

/**
 * Created by Luuk on 3-10-2016.
 */

public class RandomUser implements Serializable {

    public String mUserThumbnail, mTitle, mFirstName, mLastName, mEmail, mPhoneNumber, mCellNumber,
    mStreet, mCity, mState;
    public int mPostcode;

    // Getters & Setters
    public String getUserThumbnail() {
        return mUserThumbnail;
    }

    public void setUserThumbnail(String userThumbnail) {
        this.mUserThumbnail = userThumbnail;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        this.mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.mPhoneNumber = phoneNumber;
    }

    public String getCellNumber() {
        return mCellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.mCellNumber = cellNumber;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        this.mStreet = street;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        this.mCity = city;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        this.mState = state;
    }

    public int getPostcode() {
        return mPostcode;
    }

    public void setPostcode(int postCode) {
        this.mPostcode = postCode;
    }

    @Override
    public String toString() {
        return "User {"             +
                "Thumbnail: '"      + mUserThumbnail    + '\'' +
                "Title: '"          + mTitle            + '\'' +
                "First name: '"     + mFirstName        + '\'' +
                "Last name: '"      + mLastName         + '\'' +
                "Email: '"          + mEmail            + '\'' +
                "PhoneNumber: '"    + mPhoneNumber      + '\'' +
                "CellNumber: '"     + mCellNumber       + '\'' +
                "Street: '"         + mStreet           + '\'' +
                "City: '"           + mCity             + '\'' +
                "State: '"          + mState            + '\'' +
                "Postcode: '"       + mPostcode         + '\'' +
                "}";
    }

    public String getRandomUser() {
        return mFirstName + mLastName;
    }

}
