package com.luukros.contactcard;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Luuk on 3-10-2016.
 */

public class RandomUserAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList mRandomUserArrayList;

    public RandomUserAdapter(Context context, LayoutInflater layoutInflater, ArrayList<RandomUser> randomUserArrayList) {
        mContext = context;
        mLayoutInflater = layoutInflater;
        mRandomUserArrayList = randomUserArrayList;
    }

    @Override
    public int getCount() {
        int size = mRandomUserArrayList.size();
        return size;
    }

    @Override
    public Object getItem(int position) {
        return mRandomUserArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = mLayoutInflater.inflate(R.layout.listview_row, null);

            viewHolder = new ViewHolder();
            viewHolder.userThumbnail = (ImageView) convertView.findViewById(R.id.userThumbnail);
            viewHolder.userTitle = (TextView) convertView.findViewById(R.id.userTitle);
            viewHolder.userFirstName = (TextView) convertView.findViewById(R.id.userFirstName);
            viewHolder.userLastName = (TextView) convertView.findViewById(R.id.userLastName);
            viewHolder.userEmail = (TextView) convertView.findViewById(R.id.userEmail);
            Log.i("getView()", "viewHolder.addedStuff");

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        RandomUser randomUser = (RandomUser) mRandomUserArrayList.get(position);

        Picasso.with(mContext).load(randomUser.mUserThumbnail).into(viewHolder.userThumbnail);
        viewHolder.userTitle.setText(randomUser.mTitle);
        viewHolder.userFirstName.setText(randomUser.mFirstName);
        viewHolder.userLastName.setText(randomUser.mLastName);
        viewHolder.userEmail.setText(randomUser.mEmail);

        return convertView;
    }

    private static class ViewHolder {
        public ImageView userThumbnail;
        public TextView userTitle;
        public TextView userFirstName;
        public TextView userLastName;
        public TextView userEmail;
    }
}
